# README #

README for ARDemo repository.

### DESCRIPTION ###

This repository contains the source code used in the Intro to AR HLS. The first demo scene, rendering a simple cube at a hit result, 
UnityARKitScene.unity can be found at Assets/UnityARKitPlugin/Examples/UnityARKitScene. The second demo scene, tracking the app state,
rendering a lamp at a hit result and toggling it on and off with touch, Main.unity can be found at Assets/Scenes. The scripts for the 
controllers for the second scene are at Assets/Scripts. These combine the code and logic from some of the boilerplate
ARKit Plugin scripts for hit testing and plane detection into custom controllers.

### GET SET UP ###

Requirements:
1) Unity 5.6.2 or later  

2) iOS 11+  

3) Xcode 9 beta or later  

4) iPhone 6S or later, iPad 2017 or later  

  
    
	
How to set up and build project:  

1) Open project in Unity  

2) Navigate to either UnityARKitScene or Main scene in scene view  

3) Open Build Settings (File --> Build Settings)  

4) Choose iOS and click Switch Platform  

5) Open Player Settings  

6) Add description to Camera Usage Description (your app WILL crash if this is empty!!)  

7) Update minimum iOS version to 11.0  

8) Save and build  

9) Open XCode and make sure you have unique bundle identifier and proper provisioning profiles  

10) Test on device!   

