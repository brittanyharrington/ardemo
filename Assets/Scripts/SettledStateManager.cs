﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;

public class SettledStateManager : MonoBehaviour {
	
	[HideInInspector]
	public GameObject Furniture;
	public GameObject DirectionalLight;
	private BoxCollider[] _colliders;
	bool lightOn = true;

	// Use this for initialization
	void Start () {
	}

	private void OnEnable()
	{
		//get colliders on furniture to detect object touch
		_colliders = Furniture.GetComponentsInChildren<BoxCollider>();
	}

	// Update is called once per frame
	void Update ()
	{
		//only start checking for touch only the object is placed and settled
		if (AppManager.Instance.ARExperinceState != ARExperinceState.Settled)
		{
			enabled = false;
			return; 
		}
		
		CheckTouch();
	}

	void CheckTouch()
	{
		if (Input.touchCount == 0)
			return;

		var touch = Input.GetTouch(0);

		if (touch.phase == TouchPhase.Began)
			CastRay(new Vector3(touch.position.x, touch.position.y, 0));

		DirectionalLight.SetActive(lightOn);

	}
	
	void CastRay(Vector3 screenPosition)
	{
		var ray = Camera.main.ScreenPointToRay(screenPosition);
		RaycastHit hitInfo;
		
		if (Physics.Raycast(ray, out hitInfo) && _colliders.Contains(hitInfo.collider))
		{
			//toggle the light on and off on tap
			if (lightOn) {
				lightOn = false;
			} else {
				lightOn = true;
			}
		}
	}

}
