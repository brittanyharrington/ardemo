﻿using System;
using UnityEngine;

public class StatusDisplayManager : MonoBehaviour
{
	//Not a necessary class: used to display current state of application (i.e. tracking, placing on plane)
	public GameObject InitializingTrackingGO;
	public GameObject PlacementGO;
	public GameObject SettledGO;
	public GameObject TrackingIssuesGO;

	private GameObject[] StatusDisplayGOs;
	
	// Use this for initialization
	void Start ()
	{
		StatusDisplayGOs = new[]
		{
			InitializingTrackingGO, 
			PlacementGO, 
			SettledGO,
			TrackingIssuesGO
		};

		foreach (var child in StatusDisplayGOs)
			child.SetActive(false);
	}

	private ARExperinceState? _previousARExperinceState;
	
	// Update is called once per frame
	void Update ()
	{
		if (AppManager.Instance.ARExperinceState == _previousARExperinceState)
			return;

		_previousARExperinceState = AppManager.Instance.ARExperinceState;
		
		GameObject toActivate;
		
		switch (AppManager.Instance.ARExperinceState)
		{
			case ARExperinceState.Init:
			case ARExperinceState.PlaneSearch:
				toActivate = InitializingTrackingGO;
				break;
			case ARExperinceState.Placement:
				toActivate = PlacementGO;
				break;
			case ARExperinceState.Settled:
				toActivate = SettledGO;
				break;
			case ARExperinceState.InsufficientFeatures:
				toActivate = TrackingIssuesGO;
				break;
			default:
				toActivate = InitializingTrackingGO;
				break;
		}

		foreach (var child in StatusDisplayGOs)
			child.SetActive(child == toActivate);
	}
}
