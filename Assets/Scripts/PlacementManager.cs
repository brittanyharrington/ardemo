﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.XR.iOS;

public class PlacementManager : MonoBehaviour
{
	[HideInInspector]
	public GameObject ItemToPlace;
	
	private Rigidbody _furnitureRigidBody;
	private bool _settling = false;

	// Use this for initialization
	void Start () 
	{
	}

	// Update is called once per frame
	void Update ()
	{

		if (!_settling)
		{
			TouchHitTest();
			return;
		}
		
		if (_furnitureRigidBody.IsSleeping())
		{
			AppManager.Instance.PlacementComplete();
		}
	}

	/// <summary>
	/// Performs hit testing on the device using ARKit' hit testing
	/// </summary>
	/// <exception cref="ArgumentOutOfRangeException"></exception>
	private void TouchHitTest()
	{
		//Check if there' any finger touching the screen
		if (Input.touchCount == 0)
			return;

		var touch = Input.GetTouch(0);
		var arpoint = GetTouchAsARPoint(touch);

		switch (touch.phase)
		{
			case TouchPhase.Began:
			case TouchPhase.Moved:
			{
				//Hit test and place object at hit coordinates
				var result = GetFirstHit(arpoint);

				if (result.HasValue)
					MoveItemToPosition(UnityARMatrixOps.GetPosition(result.Value.worldTransform));
				else if (ItemToPlace.activeSelf)
					ItemToPlace.SetActive(false);
			}
				break;
				
			case TouchPhase.Stationary:
				//Do nothing
				break;
			case TouchPhase.Ended:
				HandlePlacement();
				break;
			case TouchPhase.Canceled:
				HandlePlacementCanceled();
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
	}

	/// <summary>
	/// Activates the item if needed, then moves it to the specified position, facing the camera
	/// </summary>
	/// <param name="position">The position to move the item to</param>
	private void MoveItemToPosition(Vector3 position)
	{
		if (!ItemToPlace.activeSelf)
			ItemToPlace.SetActive(true);
		
		ItemToPlace.transform.position = position;
		
		//var rotation = UnityARMatrixOps.GetRotation(result.worldTransform);
		var lookPos = Camera.main.transform.position;
		lookPos.y = position.y;
		ItemToPlace.transform.LookAt(lookPos);
	}

	/// <summary>
	/// Initiates placement
	/// </summary>
	private void HandlePlacement()
	{
		Debug.Log("HandlePlacement");
		//make sure to only call it in the right state, once.
		if (AppManager.Instance.ARExperinceState != ARExperinceState.Placement || _settling) 
			return;
		
		var actualFurniture = GameObject.FindGameObjectWithTag("furniture");
		_furnitureRigidBody = actualFurniture.gameObject.GetComponent<Rigidbody>();
		_furnitureRigidBody.useGravity = true;
		_settling = true;
	}

	/// <summary>
	/// cancels the placemenet process
	/// </summary>
	private void HandlePlacementCanceled()
	{
		if (ItemToPlace.activeSelf)
			ItemToPlace.SetActive(false);
	}

	/// <summary>
	/// Performs a hit test and returns the first valid hit test result, or null if there is no such result
	/// </summary>
	/// <param name="point"></param>
	/// <returns></returns>
	private ARHitTestResult? GetFirstHit(ARPoint point)
	{
		//only look for hitResults of type "ExistingPlaneUsingExtent" to avoid infinite planes
		var hitTestResults = UnityARSessionNativeInterface.GetARSessionNativeInterface()
			.HitTest(point, ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent);

		return hitTestResults.Cast<ARHitTestResult?>().FirstOrDefault(f => f.HasValue && f.Value.isValid);
	}

	private ARPoint GetTouchAsARPoint(Touch touch)
	{
		//get 2D coordinates of touch
		var screenPosition = Camera.main.ScreenToViewportPoint(touch.position);
		var result = new ARPoint
		{
			x = screenPosition.x, 
			y = screenPosition.y
		};

		return result;
	}
}
