﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.iOS;

public enum ARExperinceState
{
	//Keep track of possible states: initializing plane, plane detected, placing object, object settled
	Init, 
	InsufficientFeatures, 
	PlaneSearch, 
	Placement, 
	Settled
}

public class AppManager : MonoBehaviour
{
	private ARExperinceState _furthestARExperinceState = ARExperinceState.Init;

	public ARExperinceState _ARExperinceState;
	public ARExperinceState ARExperinceState
	{
		get { return _ARExperinceState; }
		set
		{
			_ARExperinceState = value;
			if (value > _furthestARExperinceState)
				_furthestARExperinceState = value;

			Debug.Log("ARExperinceState: " + value);
		}
	}

	public static AppManager Instance { get; private set; }

	private void Awake()
	{
		if (Instance != null)
			throw new Exception("Multiple instances of AppManager are not allowed");
		
		Instance = this;
	}

	public bool IsPlacementComplete;

	private UnityARAnchorManager ARAnchorManager;
	public GameObject PlanePrefab;
	public GameObject Furniture;


	// Use this for initialization
	void Start () 
	{
		//Plane initialization (same as GeneratePlanes code)
		UnityARSessionNativeInterface.ARSessionTrackingChangedEvent += TrackingChaged; 
		GetComponent<UnityARCameraManager>().enabled = true;
		UnityARUtility.InitializePlanePrefab(PlanePrefab);

		if (ARAnchorManager == null)
			ARAnchorManager = new UnityARAnchorManager();
	}

	

	private ARTrackingStateReason? _previousTrackingStateReason;

	private void TrackingChaged(UnityARCamera arCamera)
	{
		if (_previousTrackingStateReason == arCamera.trackingReason)
			return;

		Debug.Log("Tracking Changed: " + arCamera.trackingReason);
		
		switch (arCamera.trackingReason)
		{
			case ARTrackingStateReason.ARTrackingStateReasonInitializing:
				ARExperinceState = ARExperinceState.Init;
				break;
			
			case ARTrackingStateReason.ARTrackingStateReasonInsufficientFeatures:
				ARExperinceState = ARExperinceState.InsufficientFeatures;
				break;
				
			case ARTrackingStateReason.ARTrackingStateReasonNone:
				if (
				    _previousTrackingStateReason == ARTrackingStateReason.ARTrackingStateReasonInsufficientFeatures)
					ARExperinceState = _furthestARExperinceState;
				else  //Init finished
					ARExperinceState = ARExperinceState.PlaneSearch;
				break;
			
			default:
				throw new ArgumentOutOfRangeException();
		}
		
		_previousTrackingStateReason = arCamera.trackingReason;
	}

	public void PlacementComplete()
	{
		ARExperinceState = ARExperinceState.Settled;

		GetComponent<PlacementManager>().enabled = false;

		var settledStateManager = GetComponent<SettledStateManager>();
			
		settledStateManager.Furniture = Furniture;
		settledStateManager.enabled = true;
		
		IsPlacementComplete = true;

	}
	
	// Update is called once per frame
	void Update () 
	{
		//Skip forward unless we just found a plane - then destroy the particle cloud
		if (ARExperinceState != ARExperinceState.PlaneSearch)
			return;
				
		if (ARAnchorManager == null || ARAnchorManager.GetCurrentPlaneAnchors().Count == 0)
			return;
		
		ARExperinceState = ARExperinceState.Placement;
		Destroy(GetComponent<UnityPointCloudExample>());

		GetComponent<PlacementManager>().ItemToPlace = Furniture;
		GetComponent<PlacementManager>().enabled = true;
	}

	//Allows app to exit gracefully and not crash upon re-entry
	private void OnDestroy()
	{
		UnityARSessionNativeInterface.ARSessionTrackingChangedEvent -= TrackingChaged; 
	}

	private void TurnOffWorldTracking()
	{
		ARAnchorManager.Destroy();
		ARAnchorManager = null;

		UnityARSessionNativeInterface.ARSessionTrackingChangedEvent -= TrackingChaged;

		UnityARSessionNativeInterface.GetARSessionNativeInterface().Pause();

		GetComponent<UnityARCameraManager>().enabled = false;
		Instance = null;
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus)
		{
			TurnOffWorldTracking();
		}
		else
		{
			//reload current scene
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
	}
}

