﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanePrefabController : MonoBehaviour
{
	public GameObject PlaneSearchGO;
	
	// Use this for initialization
	void Start () {
		SetActiveValues(false);
	}

	private void SetActiveValues(bool placementComplete)
	{
		PlaneSearchGO.SetActive(!placementComplete);
	}
	
	private bool _previousPlacementComplete;
	// Update is called once per frame
	void Update ()
	{
		var placementComplete = AppManager.Instance.IsPlacementComplete;
		if (_previousPlacementComplete == placementComplete)
			return;
		
		SetActiveValues(placementComplete);
		_previousPlacementComplete = placementComplete;
	}

}
